﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using B2R.Model;
using B2R.Service.Interface;
using B2R.Service;
using Xunit;

namespace B2R.Tests
{
    public class AccessToBaseTests
    {
        [Fact]
        void AddOrder()
        {
            var order = Helpers.Helpers.GetOrder();
            
            IService ser = new ServiceDTO();
            
            ser.AddOrder(order);
        }

        [Fact]
        void AddAddress()
        {
            var address = Helpers.Helpers.GetAddress();

            IService ser = new ServiceDTO();

            ser.AddAddress(address);
            
        }

        [Fact]
        void AddUser()
        {
            var user = Helpers.Helpers.GetUser();
            IService ser = new ServiceDTO();
            ser.AddUser(user);
        }
    }
}
