﻿using System;
using System.Collections.Generic;
using B2R.Model;

namespace B2R.Tests.Helpers
{
    public static class Helpers
    {
        public static Order GetOrder()
        {
            return new Order
            {
                CreateDate = DateTime.Now,
                Name = "Zamówienie2",
                User = GetUser(),
                Validity = 23,
                Valuations = GetValuations()
            };
        }

        private static ICollection<Valuation> GetValuations()
        {
            return new List<Valuation>
            {
                GetValuation1()
            };
        }

        public static Valuation GetValuation1()
        {
            return new Valuation
            {
                Description = "Zabudowa Kuchenna",
                SumDecimal = (decimal) 256.32,
                Products=GetProducts()
            };
        }

        public static ICollection<Product> GetProducts()
        {
            return new List<Product>
            {
                GetProduct1(),
                GetProduct2()
            };
        }
        
        public static Product GetProduct1()
        {
            return new Product
            {
                Name = "Płyta Laminowana",
                Unit = Unit.m2,
                Amounts = new List<Amount>
                {
                    new Amount {Length = 2.3, Width = .65},
                    new Amount {Length = 2.4, Width = .45}
                }
            };
        }

        public static Product GetProduct2()
        {
            return new Product
            {
                Name = "Szuflada łożyskowa",
                Unit = Unit.szt,
                Amounts = new List<Amount>
                {
                    new Amount {Length = 4}
                }
            };
        }

        public static User GetUser()
        {
            return new User
            {
                Name = "Piotr",
                Surname = "Wachowicz",
                Address = GetAddress(),
                Email=GetEmails(),
                Phone=GetPhones()
            };
        }

        public static Address GetAddress()
        {
            return new Address
            {
                City = "Warszawa",
                Province = "Mazowieckie",
                Street = "Nowolipie 26B/16",
                ZipCode = "01-011"
            };
        }

        public static ICollection<Email> GetEmails()
        {
            return new List<Email>
            {
                new Email
                {
                    EmailNumber="kubasimov@gmail.com",
                },
                new Email
                {
                    EmailNumber = "piotr.wachowicz79@gmail.com"
                }
            };
        }

        public static ICollection<Phone> GetPhones()
        {
            return new List<Phone>
            {
                new Phone
                {
                    PhoneNumber = "506-035-383"
                },
                new Phone
                {
                    PhoneNumber = ""
                }
            };
        } 
    }
}