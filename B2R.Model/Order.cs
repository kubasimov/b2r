﻿// B2RB2R.ModelOrder.cs    2014 12 25

using System;
using System.Collections.Generic;

namespace B2R.Model
{
    public class Order
    {
        public int OrderID { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public int Validity { get; set; }
        public virtual User User { get; set; }
        public ICollection<Valuation> Valuations { get; set; }

        public Order()
        {
            Valuations=new List<Valuation>();
        }
    }
}