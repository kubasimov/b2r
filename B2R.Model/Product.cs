﻿using System.Collections.Generic;

namespace B2R.Model
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual Valuation Valuation { get; set; }
        public virtual ICollection<Amount> Amounts { get; set; }

        public Product()
        {
            Amounts=new List<Amount>();
        }
    }
}