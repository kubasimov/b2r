﻿// B2RB2R.ModelUser.cs    2014 12 22

using System.Collections.Generic;

namespace B2R.Model
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Address Address { get; set; }
        public ICollection<Phone> Phone { get; set; }
        public ICollection<Email> Email { get; set; }
        public Role Role { get; set; } 

        public User()
        {
            Phone =new List<Phone>();
            Email=new List<Email>();
        }
    }
}