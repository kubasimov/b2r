﻿namespace B2R.Model
{
    public class Email
    {
        public int EmailId { get; set; }
        public string EmailNumber { get; set; }
        public virtual User User { get; set; }
    }
}