﻿using System.Collections.Generic;

namespace B2R.Model
{
    public class Valuation
    {
        public int ValuationId { get; set; }
        public string Description { get; set; }
        public decimal SumDecimal { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public Order Order { get; set; }

        public Valuation()
        {
            Products=new List<Product>();
        }
    }
}