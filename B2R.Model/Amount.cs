namespace B2R.Model
{
    public class Amount
    {
        public int AmountId { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
    }
}