namespace B2R.Model
{
    public class Phone
    {
        public int PhoneId { get; set; }
        public string PhoneNumber   { get; set; }
        public virtual User User { get; set; }
    }
}