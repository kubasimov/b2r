﻿// B2R.ServiceIService.cs    2014 12 25  Piotr Wachowicz

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using B2R.Model;

namespace B2R.Service.Interface
{
    public interface IService
    {
        Task<int> AddOrder(Order order);
        void DelOrder(int orderId);
        Task<IEnumerable<Order>> GetAllOrders();
        Task<Order> GetOrderById(int id);
        Task<IEnumerable<Order>> GetOrdersByDate(DateTime date);



        Task<int> AddAddress(Address address);
        Task<int> AddUser(User user);
    }
}