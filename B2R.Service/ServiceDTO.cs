﻿// B2R.ServiceService.cs    2014 12 25  Piotr Wachowicz

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using B2R.Model;
using B2R.Service.Context;
using B2R.Service.Interface;
using NLog;

namespace B2R.Service
{
    public class ServiceDTO :IService
    {
        private readonly B2RContext _ctx;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ServiceDTO()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", AppDomain.CurrentDomain.BaseDirectory);
            _ctx = new B2RContext();
            _ctx.Database.Log = s => Logger.Log(LogLevel.Debug,s);

        }
        public async Task<int> AddOrder(Order order)
        {
            _ctx.Orders.Add(order);
            return await _ctx.SaveChangesAsync();
        }

        public void DelOrder(int orderId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Order>> GetAllOrders()
        {
            return  await _ctx.Orders.ToListAsync();
        }

        public async Task<Order> GetOrderById(int id)
        {
            return await _ctx.Orders.Where(a => a.OrderID == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Order>> GetOrdersByDate(DateTime date)
        {
            return await _ctx.Orders.Where(a => a.CreateDate == date).ToListAsync();
        }

        public async Task<int> AddAddress(Address address)
        {
            _ctx.Address.Add(address);
            return await _ctx.SaveChangesAsync();
        }

        public async Task<int> AddUser(User user)
        {
            _ctx.Users.Add(user);
            return await _ctx.SaveChangesAsync();
        }
    }
}