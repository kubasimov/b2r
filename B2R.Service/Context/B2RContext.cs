﻿// B2R.ServiceB2RContext.cs    2014 12 25  Piotr Wachowicz

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using B2R.Model;

namespace B2R.Service.Context
{
    public class B2RContext :DbContext
    {
        public B2RContext() :base ("B2RContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<B2RContext>());
        }

        public DbSet<Address> Address { get; set; }
        public DbSet<Amount> Amounts { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Valuation> Valuations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}